#include "Kaleidoscope-LayerHighlighter.h"

#include "kaleidoscope/KeyAddr.h"                    // for KeyAddr
#include "kaleidoscope/plugin/LEDControl.h"          // for LEDControl
#include "kaleidoscope/plugin/LEDControl/LEDUtils.h" // for breathe_compute

namespace kaleidoscope {
  namespace plugin {

    EventHandlerResult LayerHighlighter::onSetup(void) {
      return EventHandlerResult::OK;
    }

    EventHandlerResult LayerHighlighter::onKeyEvent(KeyEvent &event) {
      if (event.key != LockLayer(layer)) {
        return EventHandlerResult::OK;
      }

      if (keyToggledOff(event.state)) {
        ::LEDControl.set_mode(::LEDControl.get_mode_index());
      }

      return EventHandlerResult::OK;
    }

    EventHandlerResult LayerHighlighter::afterEachCycle() {
      if (!Layer.isActive(layer)) {
        return EventHandlerResult::OK;
      }

      ::LEDControl.set_mode(::LEDControl.get_mode_index());
      for (auto keyAddr : KeyAddr::all()) {
        Key k = Layer.lookupOnActiveLayer(keyAddr);
        Key layer_key = Layer.getKey(layer, keyAddr);

        if (k == LockLayer(layer)) {
          cRGB lock_color = breath_compute(this->lockHue);
          ::LEDControl.setCrgbAt(keyAddr, lock_color);
        }

        if ((k != layer_key) || (k == Key_NoKey)) {
          ::LEDControl.refreshAt(keyAddr);
        } else {
          ::LEDControl.setCrgbAt(keyAddr, this->color);
        }
      }

      return EventHandlerResult::OK;
    }

  }
}
