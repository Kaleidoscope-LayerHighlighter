#pragma once

#include "kaleidoscope/KeyEvent.h"             // for KeyEvent
#include "kaleidoscope/event_handler_result.h" // for EventHandlerResult
#include "kaleidoscope/keyswitch_state.h"      // for keyToggledOn…
#include "kaleidoscope/plugin.h"               // for Plugin

namespace kaleidoscope {
  namespace plugin {
    class LayerHighlighter : public Plugin {
    public:
      LayerHighlighter(uint8_t l): color(CRGB(160, 0, 0)), lockHue(170), layer(l) {}

      cRGB color;
      uint8_t lockHue;

      EventHandlerResult onSetup(void);
      EventHandlerResult onKeyEvent(KeyEvent &event);
      // EventHandlerResult onKeyswitchEvent(Key &mapped_key, KeyAddr key_addr, uint8_t key_state);
      EventHandlerResult afterEachCycle();

    private:
      const uint8_t layer;
    };

  }
}
